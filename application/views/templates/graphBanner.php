<script src="<?php echo base_urL(); ?>js/graphBanner.js"></script>
<div class="row graphs">
	<div class="col-md-12">
		<!-- Community Graphs -->
		<h3 class>Community Data</h3>

		<div id="graphNav">
			<!-- Owl Carousel Graphs -->
			<div class="owl-carousel">
				<div id="graph1" class="graph"></div>
				<div id="graph2" class="graph"></div>
				<div id="graph3" class="graph"></div>
				<div id="graph4" class="graph"></div>
			</div>

			<div class="owl-controls">
				<div class="owl-nav">

					<button type="button" class="btn graphLeftBtn">
						<span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>
					</button>
					<button type="button" class="btn graphRightBtn">
						<span class="glyphicon glyphicon-circle-arrow-right" aria-hidden="true"></span>
					</button>

				</div>
			</div>
		</div>

	</div><!-- END Col-->
</div> <!-- END Graphs row -->
